#!/usr/bin/python3
""" To describe type of ast stat in a code """
from collections import Counter
import ast
import argparse

def create_empty_ast_type_dict():
    """
    as function name
    """
    type_of_ast = [
                'FunctionDef',
                'AsyncFunctionDef',
                'ClassDef',
                'Return',
                'Delete',
                'Assign',
                'AugAssign',
                'AnnAssign',
                'For',
                'AsyncFor',
                'While',
                'If',
                'With',
                'AsyncWith',
                'Raise',
                'Try',
                'Assert',
                #'Import',
                #'ImportFrom',
                'Global',
                'Nonlocal',
                'Expr',
                'Pass',
                'Break',
                'Continue']

    type_of_ast = ['ast.' + x for x in type_of_ast]
    ast_type_dict = {i : 0 for i in type_of_ast}
    return ast_type_dict

def code_stat_analy(body):
    """
    code stat analysis
    """
    ast_stat = create_empty_ast_type_dict()

    for node in body:
        for types in set(ast_stat):
            if isinstance(node, eval(types)):
                ast_stat[types] += 1
        if isinstance(node, ast.ClassDef):
            for node_class in node.__dict__['body']:
                for types_class in set(ast_stat):
                    if isinstance(node_class, eval(types_class)):
                        ast_stat[types_class] += 1
    return ast_stat

def main():
    """
    main function
    """
    arg_parse = argparse.ArgumentParser(description='Code Description')
    arg_parse.format_help()
    arg_parse.add_argument('filename', type=str, help='target code filename')
    args = arg_parse.parse_args()
    source_file_name = args.filename
    f = open(source_file_name, "r")
    r_node = ast.parse(f.read())

    code_stat = create_empty_ast_type_dict()
    body = r_node.__dict__['body']
    ast_type_dict = code_stat_analy(body)
    code_stat = Counter(code_stat) + Counter(ast_type_dict)
    print(dict(code_stat))


if __name__ == '__main__':
    main()
