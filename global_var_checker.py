#!/usr/bin/python3
""" To check if global var be recalled, and find it out"""
from collections import Counter
import ast
import argparse

class findNameNoteVisitor(ast.NodeVisitor):
    def __init__(self):
        self.nameList = []

    def visit_Name(self, node):
        self.nameList.append(node.id)

class globalVarNoteVisitor(ast.NodeVisitor):
    def __init__(self):
        self.globalVarList = []

    def visit_ClassDef(self, node):
        return

    def visit_FunctionDef(self, node):
        return

    def visit_Assign(self, node):
        assignFinder = findNameNoteVisitor()
        assignFinder.visit(node)
        self.globalVarList.extend(assignFinder.nameList)
        try:
            for names in node.targets:
                self.globalVarList.append(names.id)
        except:
            pass
        ast.NodeVisitor.generic_visit(self, node)

class globalVarCalledNoteVisitor(ast.NodeVisitor):
    def __init__(self):
        self.globalVarRecalledList = []

    def visit_Global(self, node):
        self.globalVarRecalledList.extend(node.names)

def main():
    """
    main function
    """
    arg_parse = argparse.ArgumentParser(description='GlobalVar Checker')
    arg_parse.format_help()
    arg_parse.add_argument('filename', type=str, help='target code filename')
    args = arg_parse.parse_args()

    # source_file_name = args.filename

    ROOTPATH = './examples/colab-py/md9.py'
    source_file_name = ROOTPATH

    f = open(source_file_name, "r")
    r_node = ast.parse(f.read())

    GLOBALVAR = []
    GLOBALVAR_recalled = []

    #find Global Var
    g = globalVarNoteVisitor()
    g.visit(r_node)
    GLOBALVAR.extend(g.globalVarList)

    #find Recalled Global Var
    r = globalVarCalledNoteVisitor()
    r.visit(r_node)
    GLOBALVAR_recalled.extend(r.globalVarRecalledList)

    #print out Recalled Global Var
    print('List of Global Variable be recalled in ClassDef and FunctionDef :', \
            set(GLOBALVAR).intersection(set(GLOBALVAR_recalled)))
    print('Use these Variable carefully, it could cause side effect.')

if __name__ == '__main__':
    main()


