#!/usr/bin/python3
""" To describe type of ast stat in a code """

from collections import Counter
import ast
from os import walk
from os.path import join

# Class_Define(10%)
# Function_Define(5%)
# AsyncFunctionDef
# Exception例外處理(5%)
# try例外處理多多使用應給予加分
# Raise例外處理多多使用應給予加分
# 破壞結構型語法(-5%)
# Continue 次數過多，使用次數過多達一定門檻給予倒扣
# break次數過多，使用次數過多達一定門檻給予倒扣

def create_empty_ast_type_dict():
    """
    as function name
    """
    type_of_ast = [
                'FunctionDef',
                'AsyncFunctionDef',
                'ClassDef',
                'Return',
                'Delete',
                'Assign',
                'AugAssign',
                'AnnAssign',
                'For',
                'AsyncFor',
                'While',
                'If',
                'With',
                'AsyncWith',
                'Raise',
                'Try',
                'Assert',
                #'Import',
                #'ImportFrom',
                'Global',
                'Nonlocal',
                'Expr',
                'Pass',
                'Break',
                'Continue']

    type_of_ast = ['ast.' + x for x in type_of_ast]
    ast_type_dict = {i : 0 for i in type_of_ast}
    return ast_type_dict

def code_stat(body):
    """
    describe code stat
    """
    ast_stat = create_empty_ast_type_dict()

    for node in body:
        # print(node)
        for types in set(ast_stat):
            if isinstance(node, eval(types)):
                # print("node: ", node, "types", eval(types) )
                ast_stat[types] += 1
                # print("ast_stat[types]", ast_stat[types])

        # ClassDef
        if isinstance(node, ast.ClassDef):
            for node_class in node.__dict__['body']:
                for types_class in set(ast_stat):
                    if isinstance(node_class, eval(types_class)):
                        # print("node_class: ", node_class, "types_class", eval(types_class))
                        ast_stat[types_class] += 1

        # FunctionDef
        if isinstance(node, ast.FunctionDef):
            for node_FunctionDef in node.__dict__['body']:
                for types_class in set(ast_stat):
                    if isinstance(node_FunctionDef, eval(types_class)):
                        # print("node_FunctionDef: ", node_FunctionDef, "types_node_FunctionDef", eval(types_class))
                        ast_stat[types_class] += 1

        # AsyncFunctionDef
        if isinstance(node, ast.AsyncFunctionDef):
            for node_AsyncFunctionDef in node.__dict__['body']:
                for types_class in set(ast_stat):
                    if isinstance(node_AsyncFunctionDef, eval(types_class)):
                        # print("node_AsyncFunctionDef: ", node_AsyncFunctionDef, "types_node_AsyncFunctionDef", eval(types_class))
                        ast_stat[types_class] += 1

        # # Return 語法
        # if isinstance(node, ast.Return):
        #     for node_Return in node.__dict__['body']:
        #         for types_class in set(ast_stat):
        #             if isinstance(node_Return, eval(types_class)):
        #                 # print("node_Return: ", node_Return, "types_node_Return", eval(types_class))
        #                 ast_stat[types_class] += 1

        # Delete
        if isinstance(node, ast.Delete):
            for node_Delete in node.__dict__['body']:
                for types_class in set(ast_stat):
                    if isinstance(node_Delete, eval(types_class)):
                        # print("node_Delete: ", node_Delete, "types_node_Delete", eval(types_class))
                        ast_stat[types_class] += 1

        # # Assign
        # if isinstance(node, ast.Assign):
        #     for node_Assign in node.__dict__['body']:
        #         for types_class in set(ast_stat):
        #             if isinstance(node_Assign, eval(types_class)):
        #                 print("node_Assign: ", node_Assign, "types_node_Assign", eval(types_class))
        #                 ast_stat[types_class] += 1
        #
        # # AugAssign
        # if isinstance(node, ast.AugAssign):
        #     for node_AugAssign in node.__dict__['body']:
        #         for types_class in set(ast_stat):
        #             if isinstance(node_AugAssign, eval(types_class)):
        #                 print("node_AugAssign: ", node_AugAssign, "types_node_AugAssign", eval(types_class))
        #                 ast_stat[types_class] += 1
        #
        # # AnnAssign
        # if isinstance(node, ast.AnnAssign):
        #     for node_AnnAssign in node.__dict__['body']:
        #         for types_class in set(ast_stat):
        #             if isinstance(node_AnnAssign, eval(types_class)):
        #                 print("node_AnnAssign: ", node_AnnAssign, "types_node_AnnAssign", eval(types_class))
        #                 ast_stat[types_class] += 1
        #
        # # For 迴圈
        # if isinstance(node, ast.For):
        #     for node_For in node.__dict__['body']:
        #         for types_class in set(ast_stat):
        #             if isinstance(node_For, eval(types_class)):
        #                 print("node_For: ", node_For, "types_node_For", eval(types_class))
        #                 ast_stat[types_class] += 1
        #
        # # AsyncFor 迴圈
        # if isinstance(node, ast.AsyncFor):
        #     for node_AsyncFor in node.__dict__['body']:
        #         for types_class in set(ast_stat):
        #             if isinstance(node_AsyncFor, eval(types_class)):
        #                 print("node_AsyncFor: ", node_AsyncFor, "types_node_FunctionDef", eval(types_class))
        #                 ast_stat[types_class] += 1
        #
        # # While 迴圈
        # if isinstance(node, ast.While):
        #     for node_While in node.__dict__['body']:
        #         for types_class in set(ast_stat):
        #             if isinstance(node_While, eval(types_class)):
        #                 print("node_While: ", node_While, "types_node_While", eval(types_class))
        #                 ast_stat[types_class] += 1

        # # If
        # if isinstance(node, ast.If):
        #     for node_If in node.__dict__['body']:
        #         for types_class in set(ast_stat):
        #             if isinstance(node_If, eval(types_class)):
        #                 print("node_If: ", node_If, "types_node_If", eval(types_class))
        #                 ast_stat[types_class] += 1

        # # With Python 的 with 使用方法
        # if isinstance(node, ast.With):
        #     for node_With in node.__dict__['body']:
        #         for types_class in set(ast_stat):
        #             if isinstance(node_With, eval(types_class)):
        #                 # print("node_With: ", node_With, "types_node_With", eval(types_class))
        #                 ast_stat[types_class] += 1
        #
        # # AsyncWith  Python 的 AsyncWith 使用方法
        # if isinstance(node, ast.AsyncWith):
        #     for node_AsyncWith in node.__dict__['body']:
        #         for types_class in set(ast_stat):
        #             if isinstance(node_AsyncWith, eval(types_class)):
        #                 print("node_AsyncWith: ", node_AsyncWith, "types_node_AsyncWith", eval(types_class))
        #                 ast_stat[types_class] += 1
        #
        # Raise 例外處理
        if isinstance(node, ast.Raise):
            for node_Raise in node.__dict__['body']:
                for types_class in set(ast_stat):
                    if isinstance(node_Raise, eval(types_class)):
                        # print("node_Raise: ", node_Raise, "types_node_Raise", eval(types_class))
                        ast_stat[types_class] += 1

        # Try 例外處理
        if isinstance(node, ast.Try):
            for node_Try in node.__dict__['body']:
                for types_class in set(ast_stat):
                    if isinstance(node_Try, eval(types_class)):
                        # print("node_Try: ", node_Try, "types_node_Try", eval(types_class))
                        ast_stat[types_class] += 1

        # # Assert 斷言的使用
        # if isinstance(node, ast.Assert):
        #     for node_Assert in node.__dict__['body']:
        #         for types_class in set(ast_stat):
        #             if isinstance(node_Assert, eval(types_class)):
        #                 # print("node_Assert: ", node_Assert, "types_node_Assert", eval(types_class))
        #                 ast_stat[types_class] += 1

        # Global 全域變數的使用
        if isinstance(node, ast.Global):
            for node_Global in node.__dict__['body']:
                for types_class in set(ast_stat):
                    if isinstance(node_Global, eval(types_class)):
                        # print("node_Global: ", node_Global, "types_node_Global", eval(types_class))
                        ast_stat[types_class] += 1

        # # Nonlocal
        # if isinstance(node, ast.Nonlocal):
        #     for node_Nonlocal in node.__dict__['body']:
        #         for types_class in set(ast_stat):
        #             if isinstance(node_Nonlocal, eval(types_class)):
        #                 print("node_Nonlocal: ", node_Nonlocal, "types_node_Nonlocal", eval(types_class))
        #                 ast_stat[types_class] += 1

        # # Expr 正則式
        # if isinstance(node, ast.Expr):
        #     for node_Expr in node.__dict__['body']:
        #         for types_class in set(ast_stat):
        #             if isinstance(node_Expr, eval(types_class)):
        #                 # print("node_Expr: ", node_Expr, "types_node_Expr", eval(types_class))
        #                 ast_stat[types_class] += 1

        # # Pass
        # if isinstance(node, ast.Pass):
        #     for node_Pass in node.__dict__['body']:
        #         for types_class in set(ast_stat):
        #             if isinstance(node_Pass, eval(types_class)):
        #                 print("node_Pass: ", node_Pass, "types_node_Pass", eval(types_class))
        #                 ast_stat[types_class] += 1

        # Break
        if isinstance(node, ast.Break):
            for node_Break in node.__dict__['body']:
                for types_class in set(ast_stat):
                    if isinstance(node_Break, eval(types_class)):
                        # print("node_Break: ", node_Break, "types_node_Break", eval(types_class))
                        ast_stat[types_class] += 1

        # Continue
        if isinstance(node, ast.Continue):
            for node_Continue in node.__dict__['body']:
                for types_class in set(ast_stat):
                    if isinstance(node_Continue, eval(types_class)):
                        # print("node_Continue: ", node_Continue, "types_node_Continue", eval(types_class))
                        ast_stat[types_class] += 1

    return ast_stat


def code_stmt_calc(filename : str):
    CODESTAT = create_empty_ast_type_dict()
    FILENUMBER = 0
    for root, _, files in walk(ROOTPATH):
        for f in files:
            FILENUMBER += 1
            FILENAME = join(root, f)
            if FILENAME[-3:] == '.py':
                DATA = open(FILENAME,encoding="utf-8").read()
                # print('DATA: ',DATA)
                NODE = ast.parse(DATA)
                BODY = NODE.__dict__['body']
                # print('BODY: ',BODY)
                ASTTYPEDICT = code_stat(BODY)
                # print('ASTTYPEDICT: ',ASTTYPEDICT)
                CODESTAT = Counter(CODESTAT) + Counter(ASTTYPEDICT)
    print({key: value / FILENUMBER for key, value in dict(CODESTAT).items()})
    # print({key: value for key, value in dict(CODESTAT).items()})
    print('FILENUMBER = ', FILENUMBER)


if __name__ == '__main__':
    # ROOTPATH = '../flask/src/flask/'
    # ROOTPATH = './Python-master/'
    ROOTPATH = './examples/colab-py2/'
    # ROOTPATH = './colab-py2/'
    code_stmt_calc(ROOTPATH)

