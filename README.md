# Code_quality

## Introduction
Evaluating code quality by analysis construction of code

## Supported Language
- [x] Python

## How to use
```sh 
git clone https://github.com/ncu-psl/Code_Modularity.git

# python3 code_checker.py Target_Source_Code.py
python3 code_checker.py examples/cli.py
```
The result shows the construction of Target_Source_Code.py.
We will continue doing the following step to evaluate score of code.
