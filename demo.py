import astroid
import numpy as np


str1 = \
        '''
        if (1 < 2):
            if(3 < 4):
                if(5 < 6):
                    if(7 < 8):
                        print('BBBBB')
                        if(9 < 10):
                            print('AAAAA')
        for i in range(10):
            for j in range(20):
                for k in range(30):
                    print(i)

        '''


class ast_tree_analysis():
    
    def __init__(self, code):
        self.node = astroid.parse(code)
        self.layer_number = np.zeros((10,1))
        self.go_through_if(self.node)

    def go_through_if(self, node, layer_level=0):
        self.layer_number[layer_level] += 1
        if hasattr(node, 'body'):
            for i in range(len(node.body)):
                self.go_through_if(node.body[i], layer_level+1)

ast_tree = ast_tree_analysis(str1)
print(ast_tree.layer_number)
