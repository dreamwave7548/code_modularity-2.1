x = 'x'
y, z = ['y'], set('z')

def f():
    global x, y, z
    print('x :', x)
    print('y :', y)
    print('z :', z)


def main():
    f()

if __name__ == '__main__':
    main()
